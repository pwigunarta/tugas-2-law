package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/xuri/excelize/v2"
)

var list_game = map[string]string{
	"Dota 2":    "2013",
	"Minecraft": "2011",
}

func main() {
	r := gin.Default()
	r.GET("/get-game", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"list_game": list_game,
		})
	})

	r.POST("/add-game", func(c *gin.Context) {
		nama_game := c.PostForm("nama_game")
		tahun_rilis := c.PostForm("tahun_rilis")
		_, ok := list_game[nama_game]
		if !ok {
			list_game[nama_game] = tahun_rilis
			c.JSON(200, gin.H{
				"response": "data " + nama_game + " telah dimasukkan ke dalam program",
			})
		} else {
			c.JSON(200, gin.H{
				"response": "data " + nama_game + " telah ada di dalam program",
			})
		}

	})

	r.POST("/update-game", func(c *gin.Context) {
		nama_game := c.PostForm("nama_game")
		tahun_rilis := c.PostForm("tahun_rilis")
		_, ok := list_game[nama_game]
		if ok {
			list_game[nama_game] = tahun_rilis
			c.JSON(200, gin.H{
				"response": "data " + nama_game + " telah diupdate",
			})
		} else {
			c.JSON(200, gin.H{
				"response": "data " + nama_game + " tidak ada di database",
			})
		}
	})

	r.POST("/delete-game", func(c *gin.Context) {
		nama_game := c.PostForm("nama_game")
		_, ok := list_game[nama_game]
		if ok {
			delete(list_game, nama_game)
			c.JSON(200, gin.H{
				"response": "data " + nama_game + " telah dihapus",
			})
		}
	})

	r.POST("/cluster-add", func(c *gin.Context) {
		file, file_header, _ := c.Request.FormFile("input_file")
		defer file.Close()

		file_name := file_header.Filename
		file_name = fmt.Sprintf("testfile%s", filepath.Ext(file_name))
		dir, _ := os.Getwd()

		fileLocation := filepath.Join(dir, file_name)
		targetFile, _ := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
		defer targetFile.Close()

		if _, err := io.Copy(targetFile, file); err != nil {
			log.Print(err.Error())
			return
		}
		file_excel, _ := excelize.OpenFile("testfile.xlsx")
		row, _ := file_excel.Rows("Lembar1")
		log.Print(row.TotalRows())
		for i := 0; i < row.TotalRows(); i++ {
			key_game, _ := file_excel.GetCellValue("Lembar1", fmt.Sprintf("A%d", i))
			val_game, _ := file_excel.GetCellValue("Lembar1", fmt.Sprintf("B%d", i))
			list_game[key_game] = val_game
		}
		c.JSON(200, gin.H{
			"response": "data telah ditambahkan ke dalam database",
		})
	})

	r.Run(":" + os.Getenv("PORT"))
}
